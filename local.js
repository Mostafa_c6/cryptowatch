const fs = require('fs')
const cheerio = require("cheerio")
const cloudScrapper = require('cloudscraper')

const link_address = 'https://localbitcoins.com/country/IR'


function coin_data(html) {
    const $ = cheerio.load(html)

    const render_trader = (elem) => ({
        users: $(elem).find('td.column-user > a').attr('href'),
        price: $(elem).find('td.column-price').text().trim(),
        limit: $(elem).find('td.column-limit').text().trim(),
        online: $(elem).find('td.column-user > span').hasClass('online-status-online')
    })

    const sellers = $('table.table-bitcoins').eq(0)
        .find('tr.clickable')
        .map((i, elem) => render_trader(elem))
        .get()

    const buyers = $('table.table-bitcoins').eq(1)
        .find('tr.clickable')
        .map((i, elem) => render_trader(elem))
        .get()

    return {
        sellers,
        buyers
    }

}
let test = true
if (test) {
    fs.readFile('./data.txt', 'utf8', function (err, data) {
        if (err) {
            console.error(err)
        } else {
            console.log(coin_data(data))
        }
    })
} else {
    cloudScrapper.get(link_address, (err, res, body) => {
        if (err) {
            console.error(err)
        } else {
            // console.log(body);
            console.log(coin_data(body))
        }
    })
}